using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior : MonoBehaviour
{
    public GameObject fireshot;
    public Transform firepoint;
    Animator animator;
    GameObject player;
    public bool enemyDead;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        InvokeRepeating("Fire", 1, 2.5f);
        player = GameObject.Find("Spider");
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(player.transform);
    }
    void Fire()
    {
        if (!SpiderMovement.playerDead)
        {
            if (!enemyDead)
            {
                animator.SetTrigger("fire");
                Instantiate(fireshot, firepoint.position, firepoint.rotation);
            } 
        }
    }
}
