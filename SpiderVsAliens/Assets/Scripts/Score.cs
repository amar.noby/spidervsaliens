using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Transform player;
    Text text;
    public static int score;
    public static int coins;
    public static int kills;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        score = (int)player.position.z + coins + kills;
        text.text = "Score: " + score.ToString();
    }
}
