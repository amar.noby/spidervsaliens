using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPositioning : MonoBehaviour
{
    public GameObject Tree;
    Vector3 spawnPos;
    // Start is called before the first frame update
    void Start()
    {
        TreeCreation();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void TreeCreation()
    {
        spawnPos = new Vector3(Random.Range(-3, 3), 0, Random.Range(-3, 3));
        Instantiate(Tree, transform.localPosition + spawnPos, Quaternion.identity, transform);
    }
}
