using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class SpiderMovement : MonoBehaviour
{
    public CinemachineVirtualCamera VirtualCamera;
    Animator animator;
    Rigidbody rb;
    public float speed;
    public WorldPool pool;
    static public bool playerDead;
    // Start is called before the first frame update
    void Start()
    {
        VirtualCamera.Priority = 0;
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        MoveSpider();
        speed += Time.deltaTime * 0.01f;
    }
    void MoveSpider()
    {
        animator.SetTrigger("Start");
        rb.velocity = new Vector3(Input.GetAxis("Horizontal") * speed, 0, speed);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Enemy")
        {
            PlayerDead();
        }
    }
    public void PlayerDead()
    {
        animator.SetTrigger("dead");
        rb.isKinematic = true;
        SpiderMovement.playerDead = true;
    }
}
