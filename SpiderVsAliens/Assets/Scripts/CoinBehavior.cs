using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinBehavior : MonoBehaviour
{
    public float speed;
    Vector3 newPos;
    AudioSource collection;
    // Start is called before the first frame update
    void Start()
    {
        collection = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up * Time.deltaTime * speed);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            MoveCoin();
            collection.Play();
            Score.coins += 10;
        }
    }
    void MoveCoin()
    {
        newPos = new Vector3(Random.Range(-4, 4), 0.75f, transform.position.z + 15 * 8);
        transform.position = newPos;
    }
}
