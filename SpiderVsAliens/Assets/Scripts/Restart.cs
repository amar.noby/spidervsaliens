using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    // Start is called before the first frame update
    void OnEnable()
    {
        SpiderMovement.playerDead = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Score.coins = 0;
        Score.kills = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
