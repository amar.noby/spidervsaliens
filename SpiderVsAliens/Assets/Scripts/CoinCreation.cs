using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCreation : MonoBehaviour
{
    public GameObject enemy;
    public Transform player;
    Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("createCoins", 3, 10);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void createCoins()
    {
        offset = new Vector3(0, 0, 50);
        Instantiate(enemy, player.position + offset, new Quaternion(0, 180, 0, 0));
    }
}
