using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireshotBehavior : MonoBehaviour
{
    Rigidbody rb;
    public float speed;
    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Destroy(gameObject, 5);
        rb.AddRelativeForce(Vector3.forward * speed, ForceMode.Impulse);
        audioSource = GetComponent<AudioSource>();
        audioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<Animator>().SetTrigger("dead");
            other.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            SpiderMovement.playerDead = true;
            Destroy(gameObject);
        }
    }
}
