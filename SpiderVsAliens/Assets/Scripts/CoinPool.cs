using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPool : MonoBehaviour
{
    public GameObject coin;
    const int coinNumber = 8;
    GameObject[] coins = new GameObject[coinNumber];
    Vector3 spawnPosition;
    public Transform player;
    Vector3 newPos;
    private void Awake()
    {
        for (int i = 0; i < coinNumber; i++)
        {
            spawnPosition = new Vector3(Random.Range(-4, 4), 0.75f, (i + 1) * 15);
            coins[i] = Instantiate(coin, spawnPosition, Quaternion.identity);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < coinNumber; i++)
        {
            if (player.position.z - coins[i].transform.position.z > 5)
            {
                newPos = new Vector3(Random.Range(-4, 4), 0.75f, coins[i].transform.position.z + 15 * 8);
                coins[i].transform.position = newPos;

            }
        }
    }
}
