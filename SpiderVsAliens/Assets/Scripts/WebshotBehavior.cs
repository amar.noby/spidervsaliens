using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebshotBehavior : MonoBehaviour
{
    Rigidbody rb;
    public float speed;
    AudioSource audioSource;

    // Start is called before the first frame update
    void OnEnable()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.forward * speed;
        StartCoroutine(Delay());
        audioSource = GetComponent<AudioSource>();
        audioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(5);
        gameObject.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            gameObject.SetActive(false);
            other.gameObject.GetComponent<Animator>().SetTrigger("dead");
            Destroy(other.gameObject, 1);
            other.gameObject.GetComponent<EnemyBehavior>().enemyDead = true;
            Score.kills += 100;
        }
        if (other.gameObject.tag == "Fireshot")
        {
            gameObject.SetActive(false);
            Destroy(other.gameObject);
        }
    }
}
