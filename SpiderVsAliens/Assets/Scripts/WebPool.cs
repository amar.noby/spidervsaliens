using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class WebPool : MonoBehaviour
{
    public GameObject webshot;
    const int ammo = 50;
    GameObject[] webshots = new GameObject[ammo];
    public Transform firepoint;
    public GameObject StartButtonCheck;
    public GameObject RestartButton;
    public Animator animator;
    public Cooldown cooldown;
    public CinemachineVirtualCamera VirtualCamera;
    private void Awake()
    {
        for (int i = 0; i < ammo; i++)
        {
            webshots[i] = Instantiate(webshot);
            webshots[i].SetActive(false);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !StartButtonCheck.activeInHierarchy && cooldown.countdown == 0 && !SpiderMovement.playerDead)
        {
            for (int i = 0; i < ammo; i++)
            {
                if (!webshots[i].activeInHierarchy)
                {
                    animator.SetTrigger("Shoot");
                    webshots[i].transform.position = firepoint.position;
                    webshots[i].SetActive(true);
                    cooldown.countdown = 3;
                    cooldown.StartCoroutine("Cooling");
                    break;
                }
            }
        }
        if (SpiderMovement.playerDead)
        {
            RestartButton.SetActive(true);
            VirtualCamera.Priority = 12;
        }
    }
}
