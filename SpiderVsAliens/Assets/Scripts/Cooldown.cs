using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cooldown : MonoBehaviour
{
    Text text;
    public int countdown;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public IEnumerator Cooling()
    {
        if(countdown == 3)
        {
            text.text = "COOLDOWN!";
            yield return new WaitForSeconds(2);
            text.text = "";
            countdown = 0;
        }
    }
}
