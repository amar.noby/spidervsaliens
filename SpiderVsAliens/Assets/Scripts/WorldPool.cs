using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldPool : MonoBehaviour
{
    public GameObject groundTile;
    const int groundTiles = 15;
    GameObject[] world = new GameObject[groundTiles];
    private void Awake()
    {
        Instantiate(groundTile, new Vector3(0, 0, -10), Quaternion.identity);
        for (int i = 0; i < groundTiles; i++)
        {
            world[i] = Instantiate(groundTile, new Vector3(0, 0, 10 * i), Quaternion.identity);
        }
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
